/*
*  This is part of Telegram Bot Manager project
*    (https://gitlab.com/yokai.ni/tgbotmanager)
*
*  Project by yokai 
*  https://telegram.me/GoalAchiever
*/

const spawn = require('child_process').spawn;
var fs = require('fs');

var spawnedBots = [];

if (require.main) {
    var botsDir = __dirname + "/bots/enabled";
    var botList = fs.readdirSync(botsDir);
    
    for (let i = 0; i < botList.length; i++) {
        var curBotDir = botsDir + "/" + botList[i];
        
        //go to bot's directory and run "npm run" there
        var curBotIndex = spawnedBots.length;
        spawnedBots[curBotIndex] = {};
        spawnedBots[curBotIndex].name = botList[i];
        spawnedBots[curBotIndex].logfile = __dirname + "/logs/" + botList[i] + ".log";

        spawnedBots[curBotIndex].proc = 
            spawn("npm", ["start"], {
                cwd: curBotDir
            });
            
        spawnedBots[curBotIndex].proc.stdout.on('data', (data) => {
          console.log(`${spawnedBots[curBotIndex].name} stdout: ${data}`);
          fs.appendFileSync(spawnedBots[curBotIndex].logfile, `stdout: ${data}`);
        });

        spawnedBots[curBotIndex].proc.stderr.on('data', (data) => {
          console.log(`${spawnedBots[curBotIndex].name} stderr: ${data}`);
          fs.appendFileSync(spawnedBots[curBotIndex].logfile, `stderr: ${data}\n`);
        });

        spawnedBots[curBotIndex].proc.on('close', (code) => {
          console.log(`child process ${spawnedBots[curBotIndex].name} exited with code ${code}`);
          fs.appendFileSync(spawnedBots[curBotIndex].logfile, `exit: ${code}\n`);
        });
    }
} else {
}
